  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        Made with <img style="width: 15px;" src="img/love.png"> in Indonesia
      </div>
      <strong>Copyright &copy; 2016 <a target="_blank" href="http://facebook.com/fendy.kharisma">me</a>.</strong> All rights
      reserved.
    </div>
    <!-- /.container -->
  </footer>