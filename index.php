<!DOCTYPE html>
<html>
<head>
	<?php include 'header.php'; ?>
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
	<div class="wrapper">

		<?php include 'topmenu.php'; ?>

		<!-- Full Width Column -->
		<div class="content-wrapper">

			<?php
			@$_pages = $_GET['pages'];
			if (file_exists('pages/' . $_pages . '.html')) {
				echo file_get_contents('pages/' . $_pages . '.html');
			} else {
				echo file_get_contents('pages/index.html');
			}; 
			?>

			<!-- /.container -->
		</div>
		<!-- /.content-wrapper -->

		<?php include 'footer.php'; ?>
	</div>
	<!-- ./wrapper -->

	<!-- Bootstrap 3.3.6 -->
	<script src="js/bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script src="js/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="js/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="js/demo.js"></script>

	<!-- page script -->

	<!-- Facebook Plugins Comments -->
	<div id="fb-root"></div>
	<script>
	// (function(d, s, id) {
	// 	var js, fjs = d.getElementsByTagName(s)[0];
	// 	if (d.getElementById(id)) return;
	// 	js = d.createElement(s); js.id = id;
	// 	js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.8&appId=1573417136245759";
	// 	fjs.parentNode.insertBefore(js, fjs);
	// }(document, 'script', 'facebook-jssdk'));
</script>
</body>
</html>
